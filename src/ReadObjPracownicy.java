import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReadObjPracownicy {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Pracownik> l=new ArrayList<Pracownik>();
		try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream("pracownicy.ser"))){
			Object p;
			while( (p=ois.readObject())!=null)
			{    
				l.add((Pracownik)p);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (EOFException e){
			System.out.println("Koniec danych");
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		Iterator<Pracownik> it=l.iterator();
		while(it.hasNext()){
			System.out.println(it.next().toString());
		}
	}

}
