import java.io.Serializable;
import java.time.LocalDate;

public class Pracownik extends Czlowiek{
    /**
	 * 
	 */
	
	private LocalDate dataZatr;
	public Pracownik(String imie, String nazwisko, LocalDate dataUrodz, LocalDate dataZatr) {
		super(imie, nazwisko, dataUrodz);
		this.dataZatr=dataZatr;
	}

	public LocalDate getDataZatr() {
		return dataZatr;
	}

	public String toString(){
		return super.toString()+" data zatr.: "+dataZatr.toString();
	}
}
